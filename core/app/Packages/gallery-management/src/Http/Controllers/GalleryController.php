<?php
namespace GalleryManage\Http\Controllers;

use App\Http\Controllers\Controller;
use GalleryManage\Http\Requests\GalleryRequest;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use GalleryManage\Models\GalleryCategory;
use GalleryManage\Models\Gallery;
use GalleryManage\Models\GalleryImage;

// use DeviceManage\Models\Device;
use Sentinel;
use Response;
use File;
use Validator;


class GalleryController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Gallery Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Show the GALLERY  add screen to the user.
	 *
	 * @return Response
	 */
	public function addView_gallery()
	{
		return view( 'GalleryManage::gallery.add');
	}

	/**
	 * Add new GALLERY CATEGORY data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add_gallery(galleryRequest $request)
	{
		$validate = Validator::make($request->all(), ['image' => 'required|image']);

		if ($validate->fails()) {
			return redirect('admin/gallery/add')->with([ 'error' => true,
					'error.message'=> 'Image is required',
					'error.title' => 'Error!']);
		}

		$path='uploads/images/gallery';

		$file = $request->file('image');
		$extn =$file->getClientOriginalExtension();
		$destinationPath = storage_path($path);
		$fileName = 'gallery-cover-' .date('YmdHis') .  '.' . $extn;
		$file->move($destinationPath, $fileName);

		GalleryImage::create([
			'gallery_id'=> 1,
			'path'=> $path,
			'filename'=> $fileName,
			'title' => $request->title,
		]);



		return redirect('admin/gallery/add')->with(['success' => true,
				'success.message' => 'Image Added to the gallery',
				'success.title' => 'Well Done!']);
	}

	/**
	 * View GALLERY CATEGORY List View
	 *
	 * @return Response
	 */
	public function listView_gallery()
	{
		return view( 'GalleryManage::gallery.list' );
	}

	/**
	 * GALLERY CATEGORY list
	 *
	 * @return Response
	 */
	public function jsonList_gallery(Request $request)
	{
		if($request->ajax()){
			$galleries= GalleryImage::where('gallery_id','=',1)->get();
			$jsonList = array();
			$i=1;
			foreach ($galleries as $key => $gallery) {

				$dd = array();
				array_push($dd, $i);
        array_push($dd,$gallery->title);
				array_push($dd,"<img src='".asset("core/storage/".$gallery->path."/".$gallery->filename)."' width='50px' height='50px'/>");


				$permissions = Permission::whereIn('name',['gallery.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('admin/gallery/edit/'.$gallery->id).'\'" data-toggle="tooltip" data-placement="top" title="Gallery "><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['gallery.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="gallery-delete" data-id="'.$gallery->id.'" data-toggle="tooltip" data-placement="top" title="Delete Gallery"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}

	/**
	 * Delete a GALLERY CATEGORY
	 * @param  Request $request series id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete_gallery(Request $request)
	{
			$this->validate($request,['id' => 'required|exists:sa_gallery_image,id']);
			$id = $request->input('id');
			$gallery = GalleryImage::find($id);
      $gallery->delete();
			return response()->json(['status' => 'success']);

	}

	/**
	 * Show the GALLERY CATEGORY edit screen to the series.
	 *
	 * @return Response
	 */
	public function editView_gallery($id)
	{
		$gallery=GalleryImage::findOrFail($id);

		return view('GalleryManage::gallery.edit' )->with(['gallery'=>$gallery]);
	}

	/**
	 * Add new GALLERY CATEGORY data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit_gallery(GalleryRequest $request, $id)
	{
		$validate = Validator::make($request->all(), ['image' => 'nullable|image']);

		if ($validate->fails()) {
			return redirect('admin/gallery/edit/'.$id)->with([ 'error' => true,
					'error.message'=> 'Image is required',
					'error.title' => 'Error!']);
		}

		$gallery = GalleryImage::findOrFail($id);

		$path='uploads/images/gallery';
		$fileName = $gallery->filename;

		if ($request->hasFile('image')) {
			$file = $request->file('image');
			$extn =$file->getClientOriginalExtension();
			$destinationPath = storage_path($path);
			$fileName = 'gallery-cover-' .date('YmdHis') .  '.' . $extn;
			$file->move($destinationPath, $fileName);
		}



		$gallery->update([
			'filename'=> $fileName,
			'title' => $request->title,
		]);

		return redirect('admin/gallery/edit/'.$id)->with(['success' => true,
				'success.message' => 'Image update to the gallery',
				'success.title' => 'Well Done!']);
	}

}

<?php

namespace DestinationManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use DestinationManage\Models\Destination;
use DestinationManage\Models\DestinationImage;
use Sentinel;
use Image;

class DestinationController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Blog Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Show the Branch add screen to the user.
     *
     * @return Response
     */
    public function addView() {
        return view('DestinationManage::add');
    }

    /**
     * Add new Branch data to database
     *
     * @return Redirect to Brach add
     */
    public function add(Request $request) {

      $this->validate($request,[
        'title' => 'required|max:255',
        'description' => 'required',
        'images' => 'array',
        'images.*' => 'mimes:jpeg,bmp,png',
        'cover_image' => 'required|mimes:jpeg,bmp,png,'
      ]);


      $title = $request->get('title');
      $description = $request->get('description');
      $file = $request->file('cover_image');
      $path = 'uploads/images/destination';
      $extn = $file->getClientOriginalExtension();
      $destinationPath = storage_path($path);
      $project_fileName = 'destination-' . date('YmdHis'). '.' . $extn;
      // $file->move($destinationPath, $project_fileName);

      $this->saveCoverImg($file,$destinationPath,$project_fileName);

      $desc = Destination::create([
        'title' => $title,
        'slug' => str_slug($title)."-".time(),
        'description' => $description,
        'cover_image' => $project_fileName,
        'created_by' => Sentinel::getUser()->id
      ]);

      if ($request->hasFile('images')) {
        $this->saveImages($request,$desc);
      }


      return redirect('admin/destination/add')->with(['success' => true,
        'success.message' => 'Destination Created successfully!',
        'success.title' => 'Well Done!']);
    }

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView() {
        return view('DestinationManage::list');
    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request) {
        if ($request->ajax()) {
            $data = Destination::get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $destination) {

                $dd = array();
                array_push($dd, $i);
                array_push($dd, $destination->title);
                array_push($dd,  substr(strip_tags($destination->description),0,50));

                $permissions = Permission::whereIn('name', ['destination.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('admin/destination/edit/' . $destination->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['destination.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $destination->id . '" data-toggle="tooltip" data-placement="top" title="Delete Blog"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return response()->json(array('data' => $jsonList));
        } else {
            return response()->json(array('data' => []));
        }
    }


    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request) {
      $this->validate($request,['id' => 'required|exists:destinations,id']);
      $id = $request->input('id');
      $blog = Destination::find($id);
      $blog->delete();
      return response()->json(['status' => 'success']);
    }

    public function deleteImg(Request $request)
    {
      $this->validate($request, ['key' => 'required|exists:destination_images,id']);
      $image = DestinationImage::findOrFail($request->key);
      $image->delete();
      return 1;
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
      $curblog = Destination::with('destinationImages')->findOrFail($id);
      $images = [];
      $imageCaption = [];
      foreach ($curblog->destinationImages as $key => $value) {
        array_push($images, "<img style='height:160px' src='" .url('core/storage/uploads/images/destination/'.$value->image) . "'>");
        array_push($imageCaption,array('caption'=>$value->image,'caption'=>$value->image,'key'=>$value->id,'url'=>url('admin/destination/image/delete')));
      }
      return view('DestinationManage::edit')->with(['curblog' => $curblog , 'images' => $images,'imageCaption' => $imageCaption]);
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request, $id) {

      $this->validate($request,[
        'title' => 'required|max:255',
        'description' => 'required',
        'images.*' => 'mimes:jpeg,bmp,png',
        'cover_image' => 'mimes:jpeg,bmp,png,'
      ]);

      $desc = Destination::findOrFail($id);

      $title = $request->get('title');
      $description = $request->get('description');
      $project_fileName = $desc->cover_image;

      if ($request->hasFile('cover_image')) {
        $file = $request->file('cover_image');
        $path = 'uploads/images/destination';
        $extn = $file->getClientOriginalExtension();
        $destinationPath = storage_path($path);
        $project_fileName = 'destination-' . date('YmdHis'). '.' . $extn;
        $this->saveCoverImg($file ,$destinationPath , $project_fileName);
      }



      $desc->update([
        'title' => $title,
        'slug' => str_slug($title)."-".time(),
        'description' => $description,
        'cover_image' => $project_fileName,
      ]);

      if ($request->hasFile('images')) {
        $this->saveImages($request,$desc);
      }

      return redirect('admin/destination/edit/' . $id)->with(['success' => true,
        'success.message' => 'Destination updated successfully!',
        'success.title' => 'Good Job!']);
    }

    public function saveImages($request, $desc)
    {
      foreach ($request->images as $key => $value) {

        $file = $value;
        $path = 'uploads/images/destination';
        $extn = $file->getClientOriginalExtension();
        $destinationPath = storage_path($path);
        $project_fileName = 'destination-' . date('YmdHis')."-".$key.'.' . $extn;
        $file->move($destinationPath, $project_fileName);

        $desc->destinationImages()->create([
          'image' => $project_fileName
        ]);
      }
    }

    public function saveCoverImg($file,$destinationPath,$project_fileName)
    {
      $background = \Image::canvas('430', '305');

      $image = \Image::make($file)->resize('430', '305', function ($c) {
          $c->aspectRatio();
          $c->upsize();
      });

      $background->insert($image, 'center');
      $background->save("$destinationPath/$project_fileName");
    }

}

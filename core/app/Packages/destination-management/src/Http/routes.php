<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/destination', 'namespace' => 'DestinationManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'destination.add', 'uses' => 'DestinationController@addView'
      ]);


      Route::get('edit/{id}', [
        'as' => 'destination.edit', 'uses' => 'DestinationController@editView'
      ]);

      Route::get('list', [
        'as' => 'destination.list', 'uses' => 'DestinationController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'destination.list', 'uses' => 'DestinationController@jsonList'
      ]);

      Route::delete('image/delete', [
        'as' => 'destination.image.delete', 'uses' => 'DestinationController@deleteImg'
      ]);



      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'destination.add', 'uses' => 'DestinationController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'destination.edit', 'uses' => 'DestinationController@edit'
      ]);


      Route::post('delete', [
        'as' => 'destination.delete', 'uses' => 'DestinationController@delete'
      ]);


    });
});

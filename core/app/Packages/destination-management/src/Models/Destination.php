<?php
namespace DestinationManage\Models;

use Illuminate\Database\Eloquent\Model;


class Destination extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'destinations';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title','slug','description','cover_image'];

	public function destinationImages()
	{
	  return $this->hasMany(DestinationImage::class);
	}

	
}

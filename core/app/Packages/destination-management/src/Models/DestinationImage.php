<?php
namespace DestinationManage\Models;

use Illuminate\Database\Eloquent\Model;


class DestinationImage extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'destination_images';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['destination_id','image'];

}

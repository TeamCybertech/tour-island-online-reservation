<?php

namespace DestinationManage;

use Illuminate\Support\ServiceProvider;

class DestinationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'DestinationManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('destinationManage', function($app){
            return new DestinationManage;
        });
    }
}

<?php
namespace ReviewManage\Models;

use Illuminate\Database\Eloquent\Model;
use PackageManage\Models\Package;


class Review extends Model{

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','rating','message','email','package_id','contact'];

	public function package()
	{
	  return $this->belongsTo(Package::class);
	}


}

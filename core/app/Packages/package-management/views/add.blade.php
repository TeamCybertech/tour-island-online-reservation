@extends('layouts.back.master') @section('current_title','New Package')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all" />
<style>
.rating-container .rating-stars:before {
    text-shadow: none;
}
</style>

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
<ol class="hbreadcrumb breadcrumb">
    <li><a href="{{url('admin/package/list')}}">Package Management</a></li>

    <li class="active">
        <span>New Package</span>
    </li>
</ol>
</div>
@stop
@section('content')

<div class="row">
<div class="col-lg-12">
    <div class="hpanel">
        <div class="panel-body">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul class="">
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
            </div>
          @endif
          <form  class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
              {!!Form::token()!!}

              <div class="form-group"><label class="col-sm-2 control-label">NAME</label>
                  <div class="col-sm-10"><input type="text" class="form-control" name="name"></div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label required">COVER IMAGE</label>
                  <div class="col-sm-10">
                      <input id="cover_images" name="cover_image"  type="file" class="file-loading">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label required">DURATION</label>
                  <div class="col-sm-10">
                      <input name="duration" placeholder="5 DAYS 4 NIGHTS" class="form-control"  type="text">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label required">OFFER</label>
                  <div class="col-sm-10">
                      <input name="offer"  type="checkbox" value="1">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label required">STARS</label>
                  <div class="col-sm-10">
                      <select class="form-control" name="stars">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                      </select>
                  </div>
              </div>
              <div class="form-group"><label class="col-sm-2 control-label">DESTINATION</label>
                  <div class="col-sm-10">
                     <select class="js-source-states" style="width: 100%" name="destinations[]" multiple="multiple">
                      @foreach ($destinations as $element)
                        <option value="{{$element->id}}">{{$element->title}}</option>
                      @endforeach
                    </select>
                  </div>
              </div>
              <div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
                  <div class="col-sm-10"><textarea name="description" class="form-control"></textarea></div>
              </div>

              <div class="hr-line-dashed"></div>
              <div class="form-group">
                  <div class="col-sm-8 col-sm-offset-2">
                      <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                      <button class="btn btn-primary" type="submit">Done</button>
                  </div>
              </div>

          </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>

<script type="text/javascript">
$(".js-source-states").select2();

$("#project-file").fileinput({
    allowedFileExtensions: ['jpg', 'png', 'gif'],
    previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
    'showUpload': false,
    overwriteInitial: true,
    removeIcon: '<i class="glyphicon glyphicon-trash"></i>',

});

$("#cover_images").fileinput({
    allowedFileExtensions: ['jpg', 'png', 'gif'],
    previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
    'showUpload': false,
    overwriteInitial: true,
    removeIcon: '<i class="glyphicon glyphicon-trash"></i>',

});
tinymce.init({
  selector: 'textarea',  // change this value according to your HTML
  plugins: [
      'advlist autolink lists link image charmap preview hr anchor pagebreak',
      'searchreplace wordcount visualblocks visualchars code',
      'insertdatetime media nonbreaking table contextmenu directionality',
      'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
  ],
  toolbar: 'bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify ' +
  '| bullist numlist outdent indent | link | undo redo | forecolor backcolor emoticons ',
  fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
  menubar: false
});
</script>
@stop

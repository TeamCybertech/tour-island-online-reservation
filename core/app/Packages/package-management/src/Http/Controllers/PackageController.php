<?php

namespace PackageManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use PackageManage\Models\Package;
use HotelManage\Models\Hotel;
use DestinationManage\Models\Destination;
use Sentinel;

class PackageController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Blog Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Show the Branch add screen to the user.
     *
     * @return Response
     */
    public function addView() {
      $hotels = Hotel::get();
      $destinations = Destination::get();
      return view('PackageManage::add',compact('hotels','destinations'));
    }

    /**
     * Add new Branch data to database
     *
     * @return Redirect to Brach add
     */
    public function add(Request $request) {

      $this->validate($request,[
        'name' => 'required|max:255',
        'duration' => 'required|max:255',
        'description' => 'required',
        'destinations' => 'required|array',
        'destinations.*' => 'exists:destinations,id',
        'cover_image' => 'required|mimes:jpeg,bmp,png,',
        'offer' => 'numeric|between:0,1',
        'stars' => 'numeric|between:0,5',
      ]);


      $name = $request->get('name');
      $description = $request->get('description');
      $file = $request->file('cover_image');
      $path = 'uploads/images/package';
      $extn = $file->getClientOriginalExtension();
      $destinationPath = storage_path($path);
      $project_fileName = 'package-' . date('YmdHis'). '.' . $extn;

      $this->saveCoverImg($file,$destinationPath,$project_fileName);

      $package = Package::create([
        'name' => $name,
        'slug' => str_slug($name)."-".time(),
        'description' => $description,
        'cover_image' => $project_fileName,
        'offer' => $request->offer ? 1 : 0,
        'stars' => $request->stars,
        'duration' => $request->duration,
      ]);

      $package->destinations()->sync($request->destinations);


      return redirect('admin/package/add')->with(['success' => true,
        'success.message' => 'Package Created successfully!',
        'success.title' => 'Well Done!']);
    }

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView() {
        return view('PackageManage::list');
    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request) {
        if ($request->ajax()) {
            $data = Package::get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $package) {

                $dd = array();
                array_push($dd, $i);
                array_push($dd, $package->name);
                array_push($dd,  substr(strip_tags($package->description),0,50));

                $permissions = Permission::whereIn('name', ['package.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('admin/package/edit/' . $package->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['package.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $package->id . '" data-toggle="tooltip" data-placement="top" title="Delete Blog"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return response()->json(array('data' => $jsonList));
        } else {
            return response()->json(array('data' => []));
        }
    }


    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request) {
      $this->validate($request,['id' => 'required|exists:packages,id']);
      $id = $request->input('id');
      $blog = Package::find($id);
      $blog->delete();
      return response()->json(['status' => 'success']);
    }

    public function deleteImg(Request $request)
    {
      $this->validate($request, ['key' => 'required|exists:hotel_images,id']);
      $image = HotelImage::findOrFail($request->key);
      $image->delete();
      return 1;
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
      $package = Package::with('destinations')->findOrFail($id);
      $destinations = Destination::get();
      return view('PackageManage::edit',compact('package','destinations'));
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request, $id) {

      $this->validate($request,[
        'duration' => 'required|max:255',
        'name' => 'required|max:255',
        'description' => 'required|required',
        'destinations' => 'array',
        'destinations.*' => 'exists:destinations,id',
        'cover_image' => 'mimes:jpeg,bmp,png',
        'stars' => 'numeric|between:0,5',
        'offer' => 'numeric|between:0,1'
      ]);

      $package = Package::findOrFail($id);

      $name = $request->get('name');
      $description = $request->get('description');
      $project_fileName = $package->cover_image;

      if ($request->hasFile('cover_image')) {
        $file = $request->file('cover_image');
        $path = 'uploads/images/package';
        $extn = $file->getClientOriginalExtension();
        $destinationPath = storage_path($path);
        $project_fileName = 'package-' . date('YmdHis'). '.' . $extn;
        $this->saveCoverImg($file ,$destinationPath , $project_fileName);
      }



      $package->update([
        'name' => $name,
        'slug' => str_slug($name)."-".time(),
        'description' => $description,
        'cover_image' => $project_fileName,
        'stars' => $request->stars,
        'offer' => $request->offer,
        'duration' => $request->duration,
      ]);

      $package->destinations()->sync($request->destinations, true);

      return redirect('admin/package/edit/' . $id)->with(['success' => true,
        'success.message' => 'Package updated successfully!',
        'success.title' => 'Good Job!']);
    }


    public function saveCoverImg($file,$destinationPath,$project_fileName)
    {
      $background = \Image::canvas('430', '305');

      $image = \Image::make($file)->resize('430', '305', function ($c) {
          $c->aspectRatio();
          $c->upsize();
      });

      $background->insert($image, 'center');
      $background->save("$destinationPath/$project_fileName");
    }

}

<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/package', 'namespace' => 'PackageManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'package.add', 'uses' => 'PackageController@addView'
      ]);


      Route::get('edit/{id}', [
        'as' => 'package.edit', 'uses' => 'PackageController@editView'
      ]);

      Route::get('list', [
        'as' => 'package.list', 'uses' => 'PackageController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'package.list', 'uses' => 'PackageController@jsonList'
      ]);


      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'package.add', 'uses' => 'PackageController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'package.edit', 'uses' => 'PackageController@edit'
      ]);


      Route::post('delete', [
        'as' => 'package.delete', 'uses' => 'PackageController@delete'
      ]);


    });
});

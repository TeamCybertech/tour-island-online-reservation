<?php
namespace PackageManage\Models;

use Illuminate\Database\Eloquent\Model;
use ReviewManage\Models\Review;

class Package extends Model{


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','stars','slug','description','cover_image','offer','duration'];

	public function destinations()
	{
	  return $this->belongsToMany(\DestinationManage\Models\Destination::class);
	}

	public function reviews()
	{
	  return $this->hasMany(Review::class);
	}

}

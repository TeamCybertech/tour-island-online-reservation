<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/hotel', 'namespace' => 'HotelManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'hotel.add', 'uses' => 'HotelController@addView'
      ]);


      Route::get('edit/{id}', [
        'as' => 'hotel.edit', 'uses' => 'HotelController@editView'
      ]);

      Route::get('list', [
        'as' => 'hotel.list', 'uses' => 'HotelController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'hotel.list', 'uses' => 'HotelController@jsonList'
      ]);

      Route::delete('image/delete', [
        'as' => 'hotel.image.delete', 'uses' => 'HotelController@deleteImg'
      ]);



      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'hotel.add', 'uses' => 'HotelController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'hotel.edit', 'uses' => 'HotelController@edit'
      ]);


      Route::post('delete', [
        'as' => 'hotel.delete', 'uses' => 'HotelController@delete'
      ]);


    });
});

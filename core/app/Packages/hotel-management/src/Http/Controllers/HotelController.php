<?php

namespace HotelManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use HotelManage\Models\Hotel;
use HotelManage\Models\HotelImage;
use Sentinel;
use Image;

class HotelController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Blog Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Show the Branch add screen to the user.
     *
     * @return Response
     */
    public function addView() {
        return view('HotelManage::add');
    }

    /**
     * Add new Branch data to database
     *
     * @return Redirect to Brach add
     */
    public function add(Request $request) {

      $this->validate($request,[
        'title' => 'required|max:255',
        'description' => 'required',
        'images' => 'array',
        'images.*' => 'mimes:jpeg,bmp,png',
        'cover_image' => 'required|mimes:jpeg,bmp,png,'
      ]);


      $title = $request->get('title');
      $description = $request->get('description');
      $file = $request->file('cover_image');
      $path = 'uploads/images/hotel';
      $extn = $file->getClientOriginalExtension();
      $destinationPath = storage_path($path);
      $project_fileName = 'hotel-' . date('YmdHis'). '.' . $extn;

      $this->saveCoverImg($file,$destinationPath,$project_fileName);

      $hotel = Hotel::create([
        'title' => $title,
        'slug' => str_slug($title)."-".time(),
        'description' => $description,
        'cover_image' => $project_fileName,
        'created_by' => Sentinel::getUser()->id
      ]);

      if ($request->hasFile('images')) {
        $this->saveImages($request,$hotel);
      }


      return redirect('admin/hotel/add')->with(['success' => true,
        'success.message' => 'Hotel Created successfully!',
        'success.title' => 'Well Done!']);
    }

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView() {
        return view('HotelManage::list');
    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request) {
        if ($request->ajax()) {
            $data = Hotel::get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $hotel) {

                $dd = array();
                array_push($dd, $i);
                array_push($dd, $hotel->title);
                array_push($dd,  substr(strip_tags($hotel->description),0,50));

                $permissions = Permission::whereIn('name', ['hotel.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('admin/hotel/edit/' . $hotel->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['hotel.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $hotel->id . '" data-toggle="tooltip" data-placement="top" title="Delete Blog"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return response()->json(array('data' => $jsonList));
        } else {
            return response()->json(array('data' => []));
        }
    }


    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request) {
      $this->validate($request,['id' => 'required|exists:hotels,id']);
      $id = $request->input('id');
      $blog = Hotel::find($id);
      $blog->delete();
      return response()->json(['status' => 'success']);
    }

    public function deleteImg(Request $request)
    {
      $this->validate($request, ['key' => 'required|exists:hotel_images,id']);
      $image = HotelImage::findOrFail($request->key);
      $image->delete();
      return 1;
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
      $curblog = Hotel::with('hotelImages')->findOrFail($id);
      $images = [];
      $imageCaption = [];
      foreach ($curblog->hotelImages as $key => $value) {
        array_push($images, "<img style='height:160px' src='" .url('core/storage/uploads/images/hotel/'.$value->image) . "'>");
        array_push($imageCaption,array('caption'=>$value->image,'caption'=>$value->image,'key'=>$value->id,'url'=>url('admin/hotel/image/delete')));
      }
      return view('HotelManage::edit')->with(['curblog' => $curblog , 'images' => $images,'imageCaption' => $imageCaption]);
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request, $id) {

      $this->validate($request,[
        'title' => 'required|max:255',
        'description' => 'required',
        'images.*' => 'mimes:jpeg,bmp,png',
        'cover_image' => 'mimes:jpeg,bmp,png,'
      ]);

      $hotel = Hotel::findOrFail($id);

      $title = $request->get('title');
      $description = $request->get('description');
      $project_fileName = $hotel->cover_image;

      if ($request->hasFile('cover_image')) {
        $file = $request->file('cover_image');
        $path = 'uploads/images/hotel';
        $extn = $file->getClientOriginalExtension();
        $destinationPath = storage_path($path);
        $project_fileName = 'hotel-' . date('YmdHis'). '.' . $extn;
        $this->saveCoverImg($file ,$destinationPath , $project_fileName);
      }



      $hotel->update([
        'title' => $title,
        'slug' => str_slug($title)."-".time(),
        'description' => $description,
        'cover_image' => $project_fileName,
      ]);

      if ($request->hasFile('images')) {
        $this->saveImages($request,$hotel);
      }

      return redirect('admin/hotel/edit/' . $id)->with(['success' => true,
        'success.message' => 'Hotel updated successfully!',
        'success.title' => 'Good Job!']);
    }

    public function saveImages($request, $hotel)
    {
      foreach ($request->images as $key => $value) {

        $file = $value;
        $path = 'uploads/images/hotel';
        $extn = $file->getClientOriginalExtension();
        $destinationPath = storage_path($path);
        $project_fileName = 'hotel-' . date('YmdHis')."-".$key.'.' . $extn;
        $file->move($destinationPath, $project_fileName);

        $hotel->hotelImages()->create([
          'image' => $project_fileName
        ]);
      }
    }

    public function saveCoverImg($file,$destinationPath,$project_fileName)
    {
      $background = \Image::canvas('430', '305');

      $image = \Image::make($file)->resize('430', '305', function ($c) {
          $c->aspectRatio();
          $c->upsize();
      });

      $background->insert($image, 'center');
      $background->save("$destinationPath/$project_fileName");
    }

}

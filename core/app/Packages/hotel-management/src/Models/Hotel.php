<?php
namespace HotelManage\Models;

use Illuminate\Database\Eloquent\Model;


class Hotel extends Model{


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title','slug','description','cover_image'];

	public function hotelImages()
	{
	  return $this->hasMany(HotelImage::class);
	}
}

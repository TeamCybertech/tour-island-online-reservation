<?php
namespace HotelManage\Models;

use Illuminate\Database\Eloquent\Model;


class HotelImage extends Model{


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['hotel_id','image'];

}

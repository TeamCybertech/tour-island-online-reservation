<?php
namespace BlogManage\Models;

use Illuminate\Database\Eloquent\Model;


class Blog extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_blog';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title','video_url','start_date','slug','end_date','description','home','status','image'];

	public function getImages()
	{
		return $this->hasMany('BlogManage\Models\BlogImage', 'blog_id', 'id');
	}



}

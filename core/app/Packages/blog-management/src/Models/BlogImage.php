<?php
namespace BlogManage\Models;

use Illuminate\Database\Eloquent\Model;


class BlogImage extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_blog_image';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['blog_id','path','filename'];

	

}

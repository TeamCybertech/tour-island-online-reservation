<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/blog', 'namespace' => 'BlogManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'blog.add', 'uses' => 'BlogController@addView'
      ]);
      Route::get('blog', [
        'as' => 'blog', 'uses' => 'BlogController@blog'
      ]);

      Route::get('edit/{id}', [
        'as' => 'blog.edit', 'uses' => 'BlogController@editView'
      ]);

      Route::get('list', [
        'as' => 'blog.list', 'uses' => 'BlogController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'blog.list', 'uses' => 'BlogController@jsonList'
      ]);
      Route::get('json/getSeries', [
        'as' => 'blog.list', 'uses' => 'BlogController@getSeries'
      ]);
       Route::get('json/getFeature', [
        'as' => 'blog.list', 'uses' => 'BlogController@getFeature'
      ]);
      Route::delete('image/deleteFile', [
      'as' => 'blog.edit', 'uses' => 'BlogController@jsonImageFileDelete',
    ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'blog.add', 'uses' => 'BlogController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'blog.edit', 'uses' => 'BlogController@edit'
      ]);

      Route::post('status', [
        'as' => 'blog.status', 'uses' => 'BlogController@status'
      ]);

      Route::post('delete', [
        'as' => 'blog.delete', 'uses' => 'BlogController@delete'
      ]);


    });
});

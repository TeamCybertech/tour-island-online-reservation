<?php

namespace BlogManage\Http\Controllers;

use App\Http\Controllers\Controller;
use BlogManage\Http\Requests\BlogRequest;
use Illuminate\Http\Request;
use BlogManage\Models\Blog;
use BlogManage\Models\BlogImage;
use Permissions\Models\Permission;
use Sentinel;
use Response;
use File;

class BlogController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Blog Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Show the Branch add screen to the user.
     *
     * @return Response
     */
    public function addView() {

        return view('blogManage::blog.add');
    }

    /**
     * Add new Branch data to database
     *
     * @return Redirect to Brach add
     */
    public function add(Request $request) {

      $this->validate($request,[
        'title' => 'required|max:255',
        'description' => 'required',
        'image' => 'required|image'
      ]);


      $title = $request->get('title');
      $description = $request->get('description');
      $file = $request->file('image');
      $path = 'uploads/images/blog';
      $extn = $file->getClientOriginalExtension();
      $destinationPath = storage_path($path);
      $project_fileName = 'blog-' . date('YmdHis'). '.' . $extn;
      $file->move($destinationPath, $project_fileName);


      $blog = Blog::Create([
        'title' => $title,
        'slug' => str_slug($title)."-".time(),
        'description' => $description,
        'image' => $project_fileName,
        'created_by' => Sentinel::getUser()->id
      ]);


      return redirect('admin/blog/add')->with(['success' => true,
        'success.message' => 'Post Created successfully!',
        'success.title' => 'Well Done!']);
    }

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView() {
        return view('blogManage::blog.list');
    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request) {
        if ($request->ajax()) {
            $data = Blog::with(['getImages'])->where('status', '=', 1)->get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $blog) {

                $dd = array();
                array_push($dd, $i);
                array_push($dd, $blog->title);
                array_push($dd,  substr(strip_tags($blog->description),0,50));

                $permissions = Permission::whereIn('name', ['blog.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('admin/blog/edit/' . $blog->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['blog.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $blog->id . '" data-toggle="tooltip" data-placement="top" title="Delete Blog"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }
    }

    /**
     * Activate or Deactivate user
     * @param  Request $request id with status to change
     * @return json object with status of success or failure
     */
    public function status(Request $request) {
        if ($request->ajax()) {
            $id = $request->input('id');
            $status = $request->input('status');

            $branch = Branch::find($id);
            if ($branch) {
                $branch->status = $status;
                $branch->save();
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request) {
      $this->validate($request,['id' => 'required|exists:sa_blog,id']);
      $id = $request->input('id');
      $blog = Blog::find($id);
      $blog->delete();
      return response()->json(['status' => 'success']);
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
      $curblog = Blog::findOrFail($id);
      return view('blogManage::blog.edit')->with(['curblog' => $curblog]);
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request, $id) {
      $this->validate($request,[
        'title' => 'required|max:255',
        'description' => 'required',
        'image' => 'image'
      ]);
      $blog = Blog::findOrFail($id);

      $title = $request->get('title');
      $description = $request->get('description');
      $project_fileName = $blog->image;

      if ($request->hasFile('image')) {
        $file = $request->file('image');
        $path = 'uploads/images/blog';
        $extn = $file->getClientOriginalExtension();
        $destinationPath = storage_path($path);
        $project_fileName = 'blog-' . date('YmdHis'). '.' . $extn;
        $file->move($destinationPath, $project_fileName);
      }



      $blog->update([
        'title' => $title,
        'slug' => str_slug($title)."-".time(),
        'description' => $description,
        'image' => $project_fileName,
        'created_by' => Sentinel::getUser()->id
      ]);


      return redirect('admin/blog/edit/' . $id)->with(['success' => true,
        'success.message' => 'Blog updated successfully!',
        'success.title' => 'Good Job!']);
    }

}

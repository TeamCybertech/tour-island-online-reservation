<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use ReviewManage\Models\Review;
use Mail;

class ReviewController extends Controller
{
  public function store(Request $request)
  {
    $this->validate($request , [
      'name' => 'required|max:255',
      'package_id' => 'required|exists:packages,id',
      'email' => 'required|email',
      'message' => 'required',
      'rating' => 'between:0,5',
      'contact' => 'max:12',
    ]);

    $review = Review::create($request->all());

    Mail::send('front.mail.review', ['review' => $review],function ($msq) {
			$msq->to(env('INQUIRY_EMAIL'))->subject('Review');
		});

    session()->flash('review', 'Thank you for your review');

    return response()->json(['msg' => 'Thanks for your review'],200);
  }
}

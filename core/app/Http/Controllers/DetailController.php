<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use HotelManage\Models\Hotel;
use DestinationManage\Models\Destination;
use PackageManage\Models\Package;


class DetailController extends Controller
{
    public function hotel($slug)
    {
      $hotel = Hotel::with('hotelImages')->where('slug', $slug)->first();
      $packages = Package::limit(5)->orderBy('id','desc')->get();

      if (count($hotel) == 0) {
        abort(404);
      }
      $related = Hotel::where('id','<>',$hotel->id)->limit(3)->get();

      return view('front.hotel',compact('hotel','related','packages'));
    }

    public function destination($slug)
    {
      $destination = Destination::with('destinationImages')->where('slug', $slug)->first();


      if (count($destination) == 0) {
        abort(404);
      }
      $related = Destination::where('id','<>',$destination->id)->limit(3)->get();

      return view('front.destination',compact('destination','related'));
    }
}

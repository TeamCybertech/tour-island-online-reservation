<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Permissions\Models\Permission;
use GalleryManage\Models\GalleryImage;
use PackageManage\Models\Package;
use HotelManage\Models\Hotel;
use DestinationManage\Models\Destination;
use ReviewManage\Models\Review;
use \App\Feedback;
use Mail;
use DB;

class WebController extends Controller {

	/*
		|--------------------------------------------------------------------------
		| Web Controller
		|--------------------------------------------------------------------------
		|
		| This controller renders the "marketing page" for the application and
		| is configured to only allow guests. Like most of the other sample
		| controllers, you are free to modify or remove it as you desire.
		|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('guest');
	}



	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index() {
		$packages = Package::limit(4)->orderBy('id','desc')->get();
		$hotels = Hotel::limit(4)->orderBy('id','desc')->get();
		$destinations = Destination::limit(6)->get();
		$reviews = Feedback::whereapprove(1)->limit(10)->get();

		return view('front.index',compact('packages','destinations','reviews','hotels'));
	}
	public function about() {
		return view('front.about');
	}
	public function contact() {
		$packages = Package::get();
	  return view('front.contact',compact('packages'));
	}
	public function destinations() {
	   return view('front.destinations');
	}
	public function blog()
	{
		return view('front.blog');
	}
	public function packages()
	{
		$packages = Package::orderBy('id','desc')->paginate(8);
		return view('front.packages',compact('packages'));
	}
	public function singlePackage($slug)
	{
		$package = Package::with('destinations.destinationImages', 'reviews')->where('slug',$slug)->first();

		if (count($package) == 0) {
			abort(404);
		}

		$related = Package::where('id', '<>', $package->id)->orderBy(DB::raw('RAND()'))->limit(3)->get();
		$offer = Package::where('id', '<>', $package->id)->where('offer',1)->orderBy(DB::raw('RAND()'))->limit(3)->get();
		// dd($package->destinationImages);

		return view('front.single-package',compact('package','related','offer'));
	}

	public function gallery()
	{
		$gallery = GalleryImage::orderBy('id','desc')->paginate(9);
		return view('front.gallery',compact('gallery'));
	}

	public function inquiry(Request $request)
	{
		$this->validate($request,[
			'name' => 'required|max:255',
			'email' => 'required|email',
			'package_id' => 'required|exists:packages,id',
			'message' => 'required'
		]);

		$package = Package::findOrFail($request->package_id);

		$inquiry = [
			'name' => $request->name,
			'message' => $request->message,
			'email' => $request->email,
			'contact' => $request->contact,
		];

		Mail::send('front.mail.inquiry', ['package' => $package , 'inquiry' => $inquiry],function ($msq) {
			$msq->to(env('INQUIRY_EMAIL'))->subject('Inquiry - Package');
		});
		session()->flash('msg', 'Inquiry sent successful!');
		return back();
	}

	public function hotels()
	{
		$hotels = Hotel::paginate(9);
		return view('front.hotels',compact('hotels'));
	}

	public function contactInquiry(Request $request)
	{
		$this->validate($request,[
			'name' => 'required',
			'message' => 'required',
			'email' => 'required|email',
			'type' => 'required|in:general,feedback',
			// 'package_id' => 'required_if:type,package',
			'rating' => 'required_if:type,feedback|between:0,5'
		]);

		$inquiry = [
			'name' => $request->name,
			'message' => $request->message,
			'email' => $request->email,
			'contact' => $request->contact,
			'rating' => $request->rating,
		];


		if ($request->type == 'feedback') {

			$feedback = \App\Feedback::create($inquiry);


			Mail::send('front.mail.feedback', ['feedback' => $feedback],function ($msq) {
				$msq->to(env('INQUIRY_EMAIL'))->subject('Feedback');
			});

			session()->flash('msg', 'Feedback sent successful!');
			return back();
		}


		Mail::send('front.mail.genaral-inquiry', ['inquiry' => $inquiry],function ($msq) {
			$msq->to(env('INQUIRY_EMAIL'))->subject('Inquiry - General');
		});

		session()->flash('msg', 'Inquiry sent successful!');
		return back();
	}

}

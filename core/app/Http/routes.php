<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * USER AUTHENTICATION MIDDLEWARE
 */



Route::group(['middleware' => ['auth']], function()
{
    Route::get('admin', [
      'as' => 'dashboard', 'uses' => 'WelcomeController@index'
    ]);
    Route::get('placeOrder', [
      'as' => 'index', 'uses' => 'WebController@placeOrder'
    ]);
});


  Route::get('/', [
    'as' => 'index', 'uses' => 'WebController@index'
  ]);
  Route::get('about-us', [
    'as' => 'about', 'uses' => 'WebController@about'
  ]);
  Route::get('gallery', [
    'as' => 'gallery', 'uses' => 'WebController@gallery'
  ]);
  Route::get('packages', [
    'as' => 'packages', 'uses' => 'WebController@packages'
  ]);
  Route::get('package/{slug}', [
    'as' => 'package.single', 'uses' => 'WebController@singlePackage'
  ]);
  Route::post('inquiry', [
    'as' => 'inquiry', 'uses' => 'WebController@inquiry'
  ]);

  Route::get('contact', [
    'as' => 'contact', 'uses' => 'WebController@contact'
  ]);

  Route::post('contact', [
    'as' => 'contact.inquiry', 'uses' => 'WebController@contactInquiry'
  ]);

  Route::get('blog', [
    'as' => 'blog', 'uses' => 'WebController@blog'
  ]);
  Route::get('hotels', [
    'as' => 'hotels', 'uses' => 'WebController@hotels'
  ]);

  Route::post('review', [
    'as' => 'review' , 'uses' => "ReviewController@store"
  ]);

  Route::get('hotel/{slug}', 'DetailController@hotel');
  Route::get('destination/{slug}', 'DetailController@destination');

/**
 * USER REGISTRATION & LOGIN
 */

Route::get('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@loginView'
]);
Route::post('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@login'
]);

Route::get('user/logout', [
  'as' => 'user.logout', 'uses' => 'AuthController@logout'
]);

Route::get('user/register', [
  'as' => 'user.register', 'uses' => 'AuthController@userRegisterView'
]);
Route::post('user/register', [
  'as' => 'user.register', 'uses' => 'AuthController@userRegister'
]);

Route::get('user/profile', [
  'as' => 'user.profile', 'uses' => 'WebController@profileView'
]);

/**
 * USER LOGIN VIA FACEBOOK/GOOGLE/TWITTER/LINKDIN ETC
 */


Route::get('auth/facebook', 'AuthController@redirectToFacebook');
Route::get('auth/facebook/callback', 'AuthController@handleFacebookCallback');

Route::get('auth/google', 'AuthController@redirectToGoogle');
Route::get('auth/google/callback', 'AuthController@handleGoogleCallback');

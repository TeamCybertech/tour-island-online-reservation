@if ($paginator->lastPage() > 1)
<div class="navigation paging-navigation" role="navigation">
  <ul class="page-numbers">
      @if (($paginator->currentPage() != 1))
        <li>
          <a class="next page-numbers" href="{{ $paginator->url($paginator->currentPage()-1) }}"><i class="fa fa-long-arrow-left"></i></a>
        </li>
      @endif


      @for ($i = 1; $i <= $paginator->lastPage(); $i++)
          @if (($paginator->currentPage() == $i))
            <li><span class="page-numbers current">{{ $i }}</span></li>
          @else
            <li><a class="page-numbers" href="{{ $paginator->url($i) }}">{{ $i }}</a></li>
          @endif
      @endfor

      @if (($paginator->currentPage() != $paginator->lastPage()))
        <li>
          <a class="next page-numbers" href="{{ $paginator->url($paginator->currentPage()+1) }}"><i class="fa fa-long-arrow-right"></i></a>
        </li>
      @endif

  </ul>
</div>
@endif

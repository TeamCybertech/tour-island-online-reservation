@extends('layouts.front.master') @section('title','Welcome | www.princeofgalle.com')
@section('css')
  	<link rel="stylesheet" href="{{asset('assets/front/css/booking.css')}}" type="text/css" media="all">
	<link rel="stylesheet" href="{{asset('assets/front/css/swipebox.min.css')}}" type="text/css" media="all">
	<link rel="stylesheet" href="{{asset('assets/front/css/rating.css')}}" type="text/css" media="all">

@stop
@section('content')
  <section class="single-product travel_tour-page travel_tour">
    <div class="site wrapper-content">
  		<div class="top_site_main" style="background-image:url(../assets/front/images/banner/top-heading.jpg);">
  			<div class="banner-wrapper container article_heading">
  				<div class="breadcrumbs-wrapper">
  					<ul class="phys-breadcrumb">
  						<li><a href="{{ url('/') }}" class="home">Home</a></li>
  						<li><a href="{{ url('/hotels') }}">Recommended Hotels</a></li>
  						<li>{{ $hotel->title }}</li>
  					</ul>
  				</div>
  				<h2 class="heading_primary">{{ $hotel->title }}</h2></div>
  		</div>
  		<section class="content-area single-woo-tour">
  			<div class="container">
  				<div class="tb_single_tour product">
  					<div class="top_content_single row">
  						<div class="images images_single_left">
  							<div class="title-single">
  								<div class="title">
  									<h1>{{ $hotel->title }}</h1>
  								</div>
  							</div>
  							<div class="tour_after_title">
  								<div class="tour-share">
  									<ul class="share-social">
  										<li>
  											<a target="_blank" class="facebook" href="https://www.facebook.com/sharer.php?u={{ urlencode(url('hotel/'.$hotel->slug)) }}&t={{ urlencode($hotel->title) }}"><i class="fa fa-facebook"></i></a>
  										</li>
  										<li>
  											<a target="_blank" class="twitter" href="http://twitter.com/intent/tweet?status={{ urlencode($hotel->title) }}+{{ urlencode(url('hotel/'.$hotel->slug)) }}"><i class="fa fa-twitter"></i></a>
  										</li>
  										<li>
  											<a target="_blank" class="pinterest" href="#"><i class="fa fa-pinterest"></i></a>
  										</li>
  										<li>
  											<a target="_blank" class="googleplus" href="https://plus.google.com/share?url={{ urlencode(url('hotel/'.$hotel->slug)) }}"><i class="fa fa-google"></i></a>
  										</li>
  									</ul>
  								</div>
  							</div>
  							<div id="slider" class="flexslider">
  								<ul class="slides">
                    @foreach ($hotel->hotelImages as $element)
                        <li>
      										<a href="{{ asset('core/storage/uploads/images/hotel/'.$element->image) }}" class="swipebox" title=""><img width="950" height="700" src="{{ asset('core/storage/uploads/images/hotel/'.$element->image) }}" class="attachment-shop_single size-shop_single wp-post-image" alt="" title="" draggable="false"></a>
      									</li>
                    @endforeach

  								</ul>
  							</div>
  							<div id="carousel" class="flexslider thumbnail_product">
  								<ul class="slides">
                    @foreach ($hotel->hotelImages as $element)
                        <li>
      										<img width="150" height="100" src="{{ asset('core/storage/uploads/images/hotel/'.$element->image) }}" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="" title="" draggable="false">
      									</li>
                    @endforeach
  								</ul>
  							</div>
  							<div class="clear"></div>
  							<div class="single-tour-tabs wc-tabs-wrapper">
  								<ul class="tabs wc-tabs" role="tablist">
  									<li class="description_tab active" role="presentation">
  										<a href="#tab-description" role="tab" data-toggle="tab">Description</a>
  									</li>

  								</ul>
  								<div class="tab-content">
  									<div role="tabpanel" class="tab-pane single-tour-tabs-panel single-tour-tabs-panel--description panel entry-content wc-tab active" id="tab-description">
  										<h2>Hotel Description</h2>
                      {!! $hotel->description!!}
  									</div>



  								</div>
  							</div>
  							<div class="related tours">
  								<h2>Related Hotels</h2>
  								<ul class="tours products wrapper-tours-slider">
                    @foreach ($related as $element)
                      <li class="item-tour col-md-4 col-sm-6 product">
    										<div class="item_border item-product">
    											<div class="post_images">
    												<a href="{{ url('hotel/'.$element->slug) }}">
    													<img width="430" height="305" src="{{ asset('core/storage/uploads/images/hotel/'.$element->cover_image) }}" alt="{{ $element->title }}" title="{{ $element->title }}">
    												</a>
    											</div>
    											<div class="wrapper_content">
    												<div class="post_title"><h4>
    													<a href="{{ url("hotel/$element->slug") }}" rel="bookmark">{{ $element->title }}</a>
    												</h4></div>
    												<span class="post_date">{{ $element->duration }}</span>
    												<div class="description">
    													<p>{!! substr(strip_tags($element->description), 0 , 32) !!}..</p>
    												</div>
    											</div>
    											<div class="read_more">

    												<a rel="nofollow" href="{{ url('hotel/'.$element->slug) }}" class="button product_type_tour_phys add_to_cart_button">Read more</a>
    											</div>
    										</div>
    									</li>
                    @endforeach



  								</ul>
  							</div>
  						</div>

              <div class="summary entry-summary description_single">
              <div class="affix-sidebar">
                <div class="entry-content-tour">
                  <p class="price">
                    <span class="travel_tour-Price-amount amount" style="color: #000000;">Recent Packages</span>
                  </p>
                </div>
                <div class="widget-area align-left col-sm-3">
                  <aside class="widget widget_travel_tour">
                    <div class="wrapper-special-tours">

                      @foreach ($packages as $element)
                        <div class="inner-special-tours">
                          <a href="{{ url('package/'.$element->slug) }}">
                            <img width="430" height="305" src="{{ asset('core/storage/uploads/images/package/'.$element->cover_image) }}" alt="{{ $element->name }}" title="{{ $element->name }}"></a>
                          <div class="item_rating">
                            @for ($i = 0; $i < $element->stars ; $i++)
                              <i class="fa fa-star"></i>
                            @endfor
                            @for ($i = 0; $i <  5 - $element->stars; $i++)
                              <i class="fa fa-star-o"></i>
                            @endfor
                          </div>
                          <div class="post_title"><h3>
                            <a href="{{ url('package/'.$element->slug) }}" rel="bookmark">{{ $element->name }}</a>
                          </h3></div>
                        </div>
                      @endforeach
                    </div>
                  </aside>
                </div>
              </div>
            </div>

  					</div>
  				</div>
  			</div>
  		</section>
  	</div>
  </section>
@stop
@section('scripts')

  <script type='text/javascript' src='{{ asset('assets/front/js/rating.js') }}'></script>
  <script type='text/javascript' src='{{ asset('assets/front/js/jquery.swipebox.min.js') }}'></script>
  <script type='text/javascript' src='{{ asset('assets/front/js/jquery.matchHeight.js') }}'></script>
  <script type="text/javascript">
  jQuery('#commentform').submit(function(event) {
    event.preventDefault()

    data =  jQuery(this).serializeArray()
    jQuery('#submit').attr({ 'disabled' : true});

    jQuery.ajax({
      url: '{{ url('review') }}',
      type: 'POST',
      dataType: 'json',
      data: data
    })
    .done(function(data) {
      location.reload();
    })
    .fail(function(data) {

      jQuery('#reviewError').show();

      jQuery('#reviewError').html('<ul></ul>')

      jQuery.each(data.responseJSON, function(index, val) {
        jQuery('#reviewError  ul').append('<li>'+val+'</li>')
      });
      // jQuery('#reviewError').html(data.msg);



    })


  });

    jQuery('#star-rating').rating();

    jQuery(function(){
    jQuery(document.body)
    .on('click touchend','#swipebox-slider .current img', function(e){
    return false;
    })
    .on('click touchend','#swipebox-slider .current', function(e){
    jQuery('#swipebox-close').trigger('click');
    });
    });

    jQuery(document).ready(function() {
      jQuery(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    });




  </script>
@endsection

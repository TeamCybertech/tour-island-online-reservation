@extends('layouts.front.master') @section('title','Welcome | www.princeofgalle.com')
@section('css')
	<link rel="stylesheet" href="{{ asset('assets/front/css/travel-setting.css') }}" type="text/css" media="all">

@stop
@section('content')
<section class="archive travel_tour travel_tour-page">
	<div class="site wrapper-content">
		<div class="top_site_main" style="color: rgb(255, 255, 255); background-color: rgb(0, 0, 0); background-image: url({{asset('assets/front/images/brazil.jpg')}})">
			<div class="banner-wrapper-destination container article_heading text-center">
				<h1 class="heading_primary">Tour Island Sri Lanka</h1>
				<div class="desc"><p>Discover the Sri Lanka with us</p>
				</div>

			</div>
		</div>

    <section class="content-area">
			<div class="container">
				<div class="row">
					<div class="site-main col-sm-12 full-width">
						<ul class="tours products wrapper-tours-slider">
							@foreach ($hotels as $element)
								<li class="item-tour col-md-3 col-sm-6 product">
									<div class="item_border item-product">
										<div class="post_images">
											<a href="{{ url('hotel/'.$element->slug) }}">
												@if ($element->offer)
													<span class="price">OFFER</span>
												@endif
												<img width="430" height="305" src="{{ asset('core/storage/uploads/images/hotel/'.$element->cover_image) }}" alt="{{ $element->title }}" title="{{ $element->title }}">
											</a>
											{{-- <div class="group-icon">
												<a href="tours.html" data-toggle="tooltip" data-placement="top" title="" class="frist" data-original-title="Escorted Tour"><i class="flaticon-airplane-4"></i></a>
												<a href="tours.html" data-toggle="tooltip" data-placement="top" title="" data-original-title="Rail Tour"><i class="flaticon-cart-1"></i></a>
											</div> --}}
										</div>
										<div class="wrapper_content">
											<div class="post_title"><h4>
												<a href="{{ url('hotel/'.$element->slug) }}" rel="bookmark">{{ $element->title }}</a>
											</h4></div>
											<span class="post_date">{{ $element->duration }}</span>
											<div class="description">
												<p>{!! substr(strip_tags($element->description), 0 , 32) !!}..</p>
											</div>
										</div>
										<div class="read_more">
											{{-- <div class="item_rating">
												@for ($i = 0; $i < $element->stars ; $i++)
													<i class="fa fa-star"></i>
												@endfor
												@for ($i = 0; $i <  5 - $element->stars; $i++)
													<i class="fa fa-star-o"></i>
												@endfor
											</div> --}}
											<a rel="nofollow" href="{{ url('hotel/'.$element->slug) }}" class="button product_type_tour_phys add_to_cart_button">Read more</a>
										</div>
									</div>
								</li>
							@endforeach
						</ul>



					</div>
				</div>
			</div>

      @include('includes.pagination', ['paginator' => $hotels])
		</section>

	</div>
</section>
@stop

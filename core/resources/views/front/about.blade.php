@extends('layouts.front.master') @section('title','Welcome | www.princeofgalle.com')
@section('css')
@stop
@section('content')
		<div class="site wrapper-content">
		<div class="top_site_main" style="background-image:url(assets/front/images/banner/top-heading.jpg);">
			<div class="banner-wrapper container article_heading">
				<div class="breadcrumbs-wrapper">
					<ul class="phys-breadcrumb">
						<li><a href="{{ url('/') }}" class="home">Home</a></li>
						<li>About us</li>
					</ul>
				</div>
				<h1 class="heading_primary">About Us</h1></div>
		</div>
		<section class="content-area">
			<div class="container">
				<div class="row">
					<div class="site-main col-sm-12">
						<div class="">
							<article class="type-post">

								<div class="entry-content content-thumbnail">
									<header class="entry-header">
										<h2 class="entry-title">
											<a href="single.html" rel="bookmark">Tour Island Sri Lanka</a>
										</h2>
										{{-- <div class="entry-meta">
											<span class="posted-on">Posted on <a href="single.html" rel="bookmark">
												<time class="entry-date published" datetime="2016-09-06T04:52:25+00:00">September 6, 2016</time>
											</a></span>
										</div> --}}
									</header>
									<div class="entry-desc">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam efficitur egestas risus. Sed eros augue, tempor et faucibus eu, cursus ac lacus. Ut sodales semper ante, at malesuada neque vestibulum </p>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam efficitur egestas risus. Sed eros augue, tempor et faucibus eu, cursus ac lacus. Ut sodales semper ante, at malesuada neque vestibulum </p>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam efficitur egestas risus. Sed eros augue, tempor et faucibus eu, cursus ac lacus. Ut sodales semper ante, at malesuada neque vestibulum </p>
									</div>
								</div>
							</article>

						</div>

					</div>
					{{-- <div class="widget-area col-sm-3 alignright">
						<aside class="widget widget_text">
							<img src="images/images-sidebar/sidebanner-ticketing.jpg" alt="">
						</aside>
						<aside class="widget widget_text">
							<img src="images/images-sidebar/sidebanner-tour.png" alt="">
						</aside>
						<aside class="widget widget_text">
							<img src="images/images-sidebar/hertz-sidebanner.jpg" alt="">
						</aside>
					</div> --}}
				</div>
			</div>
		</section>
	</div>
@stop

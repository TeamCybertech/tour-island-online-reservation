@extends('layouts.front.master') @section('title','Welcome | www.princeofgalle.com')
@section('css')
  <link rel="stylesheet" href="{{ asset('assets/front/css/font-linearicons.css') }}" type="text/css" media="all">
  <link rel="stylesheet" href="{{ asset('assets/front/css/swipebox.min.css') }}" type="text/css" media="all">
  <link rel="stylesheet" href="{{ asset('assets/front/css/travel-setting.css') }}" type="text/css" media="all">

@stop
@section('content')
  <div class="site wrapper-content">
		<div class="top_site_main" style="background-image:url(assets/front/images/banner/top-heading.jpg);">
			<div class="banner-wrapper container article_heading">
				<div class="breadcrumbs-wrapper">
					<ul class="phys-breadcrumb">
						<li><a href="index-2.html" class="home">Home</a></li>
						<li>Gallery</li>
					</ul>
				</div>
				<h1 class="heading_primary">Gallery</h1></div>
		</div>
		<section class="content-area">
			<div class="container">
				<div class="row">
					<div class="site-main col-sm-12 full-width">
						<div class="sc-gallery wrapper_gallery">
							{{-- <div class="gallery-tabs-wrapper filters">
								<ul class="gallery-tabs">
									<li><a href="#" data-filter="*" class="filter active">all</a></li>
									<li><a href="#" data-filter=".competitions" class="filter">Competitions</a></li>
									<li><a href="#" data-filter=".gears" class="filter">Gears</a></li>
									<li><a href="#" data-filter=".iinstructional" class="filter">Iinstructional</a></li>
									<li><a href="#" data-filter=".swimbaits" class="filter">Swimbaits</a></li>
								</ul>
							</div> --}}
							<div class="row content_gallery">
                @foreach ($gallery as $element)
                  <div class="col-sm-4 gallery_item-wrap competitions gears" data-mh="image-grp">
  									<a href="{{ asset('core/storage/uploads/images/gallery/'.$element->filename) }}" class="swipebox" title="{{ $element->title }}">
  										<img src="{{ asset('core/storage/uploads/images/gallery/'.$element->filename) }}" alt="{{ $element->title }}" class="img-responsive">
  										<div class="gallery-item">
  											<h4 class="title">{{ $element->title }}</h4>
  										</div>
  									</a>
                  </div>
                @endforeach
							</div>
						</div>
            @include('includes.pagination', ['paginator' => $gallery])
					</div>
				</div>
			</div>
		</section>
	</div>
@stop
@section('scripts')
  <script type='text/javascript' src='{{ asset('assets/front/js/jquery.swipebox.min.js') }}'></script>
  <script type='text/javascript' src='{{ asset('assets/front/js/jquery.matchHeight.js') }}'></script>
  <script type="text/javascript">
    jQuery(function(){
    jQuery(document.body)
    .on('click touchend','#swipebox-slider .current img', function(e){
    return false;
    })
    .on('click touchend','#swipebox-slider .current', function(e){
    jQuery('#swipebox-close').trigger('click');
    });
    });
  </script>
@stop

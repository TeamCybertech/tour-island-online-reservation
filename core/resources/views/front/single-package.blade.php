@extends('layouts.front.master') @section('title','Welcome | www.princeofgalle.com')
@section('css')
  	<link rel="stylesheet" href="{{asset('assets/front/css/booking.css')}}" type="text/css" media="all">
	<link rel="stylesheet" href="{{asset('assets/front/css/swipebox.min.css')}}" type="text/css" media="all">
	<link rel="stylesheet" href="{{asset('assets/front/css/rating.css')}}" type="text/css" media="all">

@stop
@section('content')
  <section class="single-product travel_tour-page travel_tour">
    <div class="site wrapper-content">
  		<div class="top_site_main" style="background-image:url(../assets/front/images/banner/top-heading.jpg);">
  			<div class="banner-wrapper container article_heading">
  				<div class="breadcrumbs-wrapper">
  					<ul class="phys-breadcrumb">
  						<li><a href="{{ url('/') }}" class="home">Home</a></li>
  						<li><a href="{{ url('/packages') }}">Packages</a></li>
  						<li>{{ $package->name }}</li>
  					</ul>
  				</div>
  				<h2 class="heading_primary">{{ $package->name }}</h2></div>
  		</div>
  		<section class="content-area single-woo-tour">
  			<div class="container">
  				<div class="tb_single_tour product">
  					<div class="top_content_single row">
  						<div class="images images_single_left">
  							<div class="title-single">
  								<div class="title">
  									<h1>{{ $package->name }}</h1>
  								</div>
  								
                  {{-- <div class="tour_code">
  									<strong>Code: </strong>LMJUYH
  								</div> --}}

  							</div>
  							<div class="tour_after_title">
  								<div class="meta_date">
  									<span style="text-transform: uppercase;">{{ $package->duration }}</span>
  								</div>

  								{{-- <div class="meta_values">
  									<span>Category:</span>
  									<div class="value">
  										<a href="tours.html" rel="tag">Escorted Tour</a>,
  										<a href="tours.html" rel="tag">Rail Tour</a>
  									</div>
  								</div> --}}

  								<div class="tour-share">
  									<ul class="share-social">
  										<li>
  											<a target="_blank" class="facebook" href="https://www.facebook.com/sharer.php?u={{ urlencode(url('package/'.$package->slug)) }}&t={{ urlencode($package->name) }}"><i class="fa fa-facebook"></i></a>
  										</li>
  										<li>
  											<a target="_blank" class="twitter" href="http://twitter.com/intent/tweet?status={{ urlencode($package->name) }}+{{ urlencode(url('package/'.$package->slug)) }}"><i class="fa fa-twitter"></i></a>
  										</li>
  										<li>
  											<a target="_blank" class="pinterest" href="#"><i class="fa fa-pinterest"></i></a>
  										</li>
  										<li>
  											<a target="_blank" class="googleplus" href="https://plus.google.com/share?url={{ urlencode(url('package/'.$package->slug)) }}"><i class="fa fa-google"></i></a>
  										</li>
  									</ul>
  								</div>
  							</div>
  							<div id="slider" class="flexslider">
  								<ul class="slides">
                    @foreach ($package->destinations as $element)
                      @foreach ($element->destinationImages as $element)
                        <li>
      										<a href="{{ asset('core/storage/uploads/images/destination/'.$element->image) }}" class="swipebox" title=""><img width="950" height="700" src="{{ asset('core/storage/uploads/images/destination/'.$element->image) }}" class="attachment-shop_single size-shop_single wp-post-image" alt="" title="" draggable="false"></a>
      									</li>
                      @endforeach
                    @endforeach

  								</ul>
  							</div>
  							<div id="carousel" class="flexslider thumbnail_product">
  								<ul class="slides">
                    @foreach ($package->destinations as $element)
                      @foreach ($element->destinationImages as $element)
                        <li>
      										<img width="150" height="100" src="{{ asset('core/storage/uploads/images/destination/'.$element->image) }}" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="" title="" draggable="false">
      									</li>
                      @endforeach
                    @endforeach
  								</ul>
  							</div>
  							<div class="clear"></div>
  							<div class="single-tour-tabs wc-tabs-wrapper">
  								<ul class="tabs wc-tabs" role="tablist">
  									<li class="description_tab active" role="presentation">
  										<a href="#tab-description" role="tab" data-toggle="tab">Description</a>
  									</li>
  									<li class="itinerary_tab_tab" role="presentation">
  										<a href="#tab-itinerary_tab" role="tab" data-toggle="tab">Destinations</a>
  									</li>
  									{{-- <li class="location_tab_tab" role="presentation">
  										<a href="#tab-location_tab" role="tab" data-toggle="tab">Location</a>
  									</li> --}}
  									<li class="reviews_tab" role="presentation">
  										<a href="#tab-reviews" role="tab" data-toggle="tab">Reviews ({{ $package->reviews()->whereapprove(1)->count() }})</a>
  									</li>
  								</ul>
  								<div class="tab-content">
  									<div role="tabpanel" class="tab-pane single-tour-tabs-panel single-tour-tabs-panel--description panel entry-content wc-tab active" id="tab-description">
  										<h2>Package Description</h2>
                      {!! $package->description!!}
  									</div>
  									<div role="tabpanel" class="tab-pane single-tour-tabs-panel single-tour-tabs-panel--itinerary_tab panel entry-content wc-tab" id="tab-itinerary_tab">
                        <ul class="tours products wrapper-tours-slider">
                          @foreach ($package->destinations as $element)
        									<li class="item-tour col-md-12 product">
        										<div class="item_border item-product">
        											<div class="post_images">
                                <center>
                                  <a class=""  href="{{ url("destination/$element->slug") }}">
          													<img width="250" height="250" src="{{ asset('core/storage/uploads/images/destination/'.$element->cover_image) }}" alt="{{ $element->title }}" title="{{ $element->title }}">
          												</a>
                                </center>


        											</div>
        											<div class="wrapper_content">
        												<div class="post_title"><h4>
        													<a href="{{ url("destination/$element->slug") }}" rel="bookmark">{{ $element->title }}</a>
        												</h4></div>
        												<div class="description">
        													{{ substr(strip_tags($element->description),0,50)}}
        												</div>
        											</div>
        										</div>
        									</li>
                        @endforeach
        								</ul>
  									</div>
  									<div role="tabpanel" class="tab-pane single-tour-tabs-panel single-tour-tabs-panel--location_tab panel entry-content wc-tab" id="tab-location_tab">
  										{{-- <div class="wrapper-gmap">
  											<div id="googleMapCanvas" class="google-map" data-lat="50.893577" data-long="-1.393483" data-address="European Way, Southampton, United Kingdom"></div>
  										</div> --}}
  									</div>
  									<div role="tabpanel" class="tab-pane single-tour-tabs-panel single-tour-tabs-panel--reviews panel entry-content wc-tab" id="tab-reviews">
  										<div id="reviews" class="travel_tour-Reviews">
  											<div id="comments">
  												<h2 class="travel_tour-Reviews-title">{{ $package->reviews()->whereapprove(1)->count() }} review for
  													<span>{{ $package->name }}</span></h2>

                          @foreach ($package->reviews()->whereapprove(1)->get() as $element)
                            <ol class="commentlist">
    													<li itemscope="" itemtype="http://schema.org/Review" class="comment byuser comment-author-physcode bypostauthor even thread-even depth-1" id="li-comment-62">
    														<div id="comment-62" class="comment_container">
    															<img alt="" src="{{ asset('assets/front/images/avata.jpg') }}" class="avatar avatar-60 photo" height="60" width="60">
    															<div class="comment-text">
    																<div class="star-rating" title="Rated 4 out of 5">
                                      @for ($i = 0; $i < $element->rating ; $i++)
                                        <i class="fa fa-star"></i>
                                      @endfor
                                      @for ($i = 0; $i <  5 - $element->rating; $i++)
                                        <i class="fa fa-star-o"></i>
                                      @endfor

    																</div>
    																<p class="meta">
    																	<strong>{{ $element->name }}</strong> –
    																	<time >{{ $element->created_at->format('F d Y') }}</time>
    																	:
    																</p>
    																<div class="description">
    																	<p>{{ $element->message }}</p>
    																</div>
    															</div>
    														</div>
    													</li>
    												</ol>
                          @endforeach

  											</div>
  											<div id="review_form_wrapper">
  												<div id="review_form">
  													<div id="respond" class="comment-respond">

  														<h3 id="reply-title" class="comment-reply-title">Add a review</h3>
                              @if (session()->has('msg-review'))
                                <div class="alert alert-success">
                                  {{session()->get('msg-review')}}
                                </div>
                              @endif
                              <div class="alert alert-danger" id="reviewError" style="display:none">

                              </div>
  														<form method="post" action="{{ url('review') }}" id="commentform" class="comment-form" novalidate="">
                                {{ csrf_field() }}
                                <input type="hidden" name="package_id" value="{{ $package->id }}">
  															<p class="comment-notes">
  																<span id="email-notes">Your email address will not be published.</span> Required fields are marked
  																<span class="required">*</span></p>
  															<p class="comment-form-author"><label for="author">Name
  																<span class="required">*</span></label>
  																<input id="author" name="name" type="text" value="" size="30" required="">
  															</p>
  															<p class="comment-form-email"><label for="email">Email
  																<span class="required">*</span></label>
  																<input id="email" name="email" type="email" value="" size="30" required="">
  															</p>
  															<p class="comment-form-email"><label for="email">Contact No.
  																<span class="required">*</span></label>
  																<input id="contact" name="contact" type="text" value="" size="30" required="">
  															</p>
  															<p class="comment-form-rating">
  																<label>Your Rating</label>
  															</p>
                                <div id="star-rating">
                                    <input type="radio" name="rating" class="rating" value="1" />
                                    <input type="radio" name="rating" class="rating" value="2" />
                                    <input type="radio" name="rating" class="rating" value="3" />
                                    <input type="radio" name="rating" class="rating" value="4" />
                                    <input type="radio" name="rating" class="rating" value="5" />
                                </div>

  															<p class="comment-form-comment">
  																<label for="comment">Your Review
  																	<span class="required">*</span></label><textarea id="comment" name="message" cols="45" rows="8" required=""></textarea>
  															</p>
  															<p class="form-submit">
  																<input name="submit" type="submit" id="submit" onclick="" class="submit" value="Submit">
  															</p></form>
  													</div>
  												</div>
  											</div>
  											<div class="clear"></div>
  										</div>
  									</div>
  								</div>
  							</div>
  							<div class="related tours">
  								<h2>Related Packages</h2>
  								<ul class="tours products wrapper-tours-slider">
                    @foreach ($related as $element)
                      <li class="item-tour col-md-4 col-sm-6 product">
    										<div class="item_border item-product">
    											<div class="post_images">
    												<a href="{{ url('package/'.$element->slug) }}">
    													<img width="430" height="305" src="{{ asset('core/storage/uploads/images/package/'.$element->cover_image) }}" alt="{{ $element->name }}" title="{{ $element->name }}">
    												</a>
    											</div>
    											<div class="wrapper_content">
    												<div class="post_title"><h4>
    													<a href="{{ url("package/$element->slug") }}" rel="bookmark">{{ $element->title }}</a>
    												</h4></div>
    												<span class="post_date">{{ $element->duration }}</span>
    												<div class="description">
    													<p>{!! substr(strip_tags($element->description), 0 , 32) !!}..</p>
    												</div>
    											</div>
    											<div class="read_more">
    												<div class="item_rating">
                              @for ($i = 0; $i < $element->stars ; $i++)
      													<i class="fa fa-star"></i>
      												@endfor
      												@for ($i = 0; $i <  5 - $element->stars; $i++)
      													<i class="fa fa-star-o"></i>
      												@endfor
    												</div>
    												<a rel="nofollow" href="{{ url('package/'.$element->slug) }}" class="button product_type_tour_phys add_to_cart_button">Read more</a>
    											</div>
    										</div>
    									</li>
                    @endforeach



  								</ul>
  							</div>
  						</div>
  						<div class="summary entry-summary description_single">
  							<div class="affix-sidebar">
  								<div class="entry-content-tour">
  									<p class="price">
  										<span class="text">Duration:</span><span class="travel_tour-Price-amount amount">{{ $package->duration }}</span>
  									</p>
  									<div class="clear"></div>
  									<div class="booking">
  										<div class="">
  											<div class="form-block__title">
  												<h4>Inquiry about package</h4>
  											</div>
                        <div class="custom_from">
                          @if (session()->has('msg'))
                            <div class="alert alert-success">
                              {{session()->get('msg')}}
                            </div>
                          @endif
                          @if ($errors->any())
                            <div class="alert alert-danger">
                              <ul class="">
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                            </div>
                          @endif
      										<div role="form" class="wpcf7" lang="en-US" dir="ltr">
      											<div class="screen-reader-response"></div>
      											<form action="{{ url('inquiry') }}" method="post" class="wpcf7-form" >
                              {!! csrf_field() !!}
                              <input type="hidden" name="package_id" value="{{ $package->id }}">
      												<p>Fill up the form below to tell us what you're looking for</p>
      												<p>
      													<span class="wpcf7-form-control-wrap your-name">
      														<input type="text" name="name" value="" size="40" class="wpcf7-form-control" required aria-invalid="false" placeholder="Your name*">
      													</span>
      												</p>
      												<p>
      													<span class="wpcf7-form-control-wrap your-email">
      														<input type="email" name="email" value="" required size="40" class="wpcf7-form-control" aria-invalid="false" placeholder="Email*">
      													</span>
      												</p>
      												<p>
      													<span class="wpcf7-form-control-wrap your-email">
      														<input type="number" name="contact" value="" required size="40" class="wpcf7-form-control" aria-invalid="false" placeholder="Contact No.">
      													</span>
      												</p>
      												<p>
      													<span class="wpcf7-form-control-wrap your-message">
      														<textarea name="message" cols="40" rows="10" required class="wpcf7-form-control" aria-invalid="false" placeholder="Message"></textarea>
      													</span>
      												</p>
      												<p>
      													<input type="submit" value="Send Enquiry" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span>
      												</p>
      											</form>
      										</div>
      									</div>

  										</div>
  									</div>

  								</div>
  								<div class="widget-area align-left col-sm-3">
  									<aside class="widget widget_travel_tour">
  										<div class="wrapper-special-tours">

                        @foreach ($offer as $element)
                          <div class="inner-special-tours">
    												<a href="{{ url('package/'.$element->slug) }}">
    													<img width="430" height="305" src="{{ asset('core/storage/uploads/images/package/'.$element->cover_image) }}" alt="{{ $element->name }}" title="{{ $element->name }}"></a>
    												<div class="item_rating">
                              @for ($i = 0; $i < $element->stars ; $i++)
      													<i class="fa fa-star"></i>
      												@endfor
      												@for ($i = 0; $i <  5 - $element->stars; $i++)
      													<i class="fa fa-star-o"></i>
      												@endfor
    												</div>
    												<div class="post_title"><h3>
    													<a href="{{ url('package/'.$element->slug) }}" rel="bookmark">{{ $element->name }}</a>
    												</h3></div>
    											</div>

                        @endforeach


  										</div>
  									</aside>
  								</div>
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>
  		</section>
  	</div>
  </section>
@stop
@section('scripts')

  <script type='text/javascript' src='{{ asset('assets/front/js/rating.js') }}'></script>
  <script type='text/javascript' src='{{ asset('assets/front/js/jquery.swipebox.min.js') }}'></script>
  <script type='text/javascript' src='{{ asset('assets/front/js/jquery.matchHeight.js') }}'></script>
  <script type="text/javascript">
  jQuery('#commentform').submit(function(event) {
    event.preventDefault()

    data =  jQuery(this).serializeArray()
    jQuery('#submit').attr({ 'disabled' : true});

    jQuery.ajax({
      url: '{{ url('review') }}',
      type: 'POST',
      dataType: 'json',
      data: data
    })
    .done(function(data) {
      location.reload();
    })
    .fail(function(data) {

      jQuery('#reviewError').show();

      jQuery('#reviewError').html('<ul></ul>')

      jQuery.each(data.responseJSON, function(index, val) {
        jQuery('#reviewError  ul').append('<li>'+val+'</li>')
      });
      // jQuery('#reviewError').html(data.msg);



    })


  });

    jQuery('#star-rating').rating();

    jQuery(function(){
    jQuery(document.body)
    .on('click touchend','#swipebox-slider .current img', function(e){
    return false;
    })
    .on('click touchend','#swipebox-slider .current', function(e){
    jQuery('#swipebox-close').trigger('click');
    });
    });

    jQuery(document).ready(function() {
      jQuery(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    });




  </script>
@endsection

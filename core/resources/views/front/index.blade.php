@extends('layouts.front.master') @section('title','Welcome | www.princeofgalle.com')
@section('css')
@stop
@section('content')
<div class="site wrapper-content">
    <div class="home-content" role="main">
        <div class="wrapper-bg-video">
            <video poster="{{asset('assets/front/images/video_slider.jpg')}}" playsinline autoplay muted loop>
                <!-- <source src="http://physcode.com/video/330149744.mp4" type="video/mp4"> -->
            </video>
            <div class="content-slider">
                <p>Find your special tour today with</p>
                <h2>Tour Island Sri Lanka</h2>
                <p><a href="tours.html" class="btn btn-slider">VIEW TOURS </a></p>
            </div>
        </div>
        <!-- <div class="slider-tour-booking">
            <div class="container">
                <div class="travel-booking-search hotel-booking-search travel-booking-style_1">
                    <form name="hb-search-form" action="http://html.physcode.com/travel/tours.html" id="tourBookingForm">
                        <ul class="hb-form-table">
                            <li class="hb-form-field">
                                <div class="hb-form-field-input hb_input_field">
                                    <input type="text" name="name_tour" value="" placeholder="Tour name">
                                </div>
                            </li>

                            <li class="hb-form-field">
                                <div class="hb-form-field-input hb_input_field">
                                    <select name="tourtax[pa_destination]">
                                        <option value="0">Destination</option>
                                        <option value="china">Brazil</option>
                                        <option value="canada">Canada</option>
                                        <option value="cuba">Cuba</option>
                                        <option value="italy">Italy</option>
                                        <option value="philippines">Philippines</option>
                                        <option value="usa">USA</option>
                                    </select></div>
                            </li>
                            <li class="hb-form-field">
                                <div class="hb-form-field-input hb_input_field">
                                    <select name="tourtax[pa_month]">
                                        <option value="0">Month</option>
                                        <option value="january">January</option>
                                        <option value="february">February</option>
                                        <option value="march">March</option>
                                        <option value="april">April</option>
                                        <option value="may">May</option>
                                        <option value="june">June</option>
                                        <option value="july">July</option>
                                        <option value="august">August</option>
                                        <option value="september">September</option>
                                        <option value="october">October</option>
                                        <option value="november">November</option>
                                        <option value="december">December</option>
                                    </select></div>
                            </li>
                            <li class="hb-submit">
                                <button type="submit">Search Tours</button>
                            </li>
                        </ul>
                        <input type="hidden" name="lang" value="">

                    </form>
                </div>
            </div>
            </div> -->
        <div class="container two-column-respon mg-top-6x mg-bt-6x">
            <div class="row">
                <div class="col-sm-12 mg-btn-6x">
                    <div class="shortcode_title title-center title-decoration-bottom-center">
                        <h3 class="title_primary">WHY CHOOSE US?</h3>
                        <span class="line_after_title"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="wpb_column col-sm-3">
                    <div class="widget-icon-box widget-icon-box-base iconbox-center">
                        <div class="boxes-icon circle" style="font-size:30px;width:80px; height:80px;line-height:80px">
                            <span class="inner-icon"><i class="vc_icon_element-icon flaticon-transport-6"></i></span>
                        </div>
                        <div class="content-inner">
                            <div class="sc-heading article_heading">
                                <h4 class="heading__primary">Diverse Destinations</h4>
                            </div>
                            <div class="desc-icon-box">
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column col-sm-3">
                    <div class="widget-icon-box widget-icon-box-base iconbox-center">
                        <div class="boxes-icon " style="font-size:30px;width:80px; height:80px;line-height:80px">
                            <span class="inner-icon"><i class="vc_icon_element-icon flaticon-sand"></i></span>
                        </div>
                        <div class="content-inner">
                            <div class="sc-heading article_heading">
                                <h4 class="heading__primary">Value for Money</h4>
                            </div>
                            <div class="desc-icon-box">
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column col-sm-3">
                    <div class="widget-icon-box widget-icon-box-base iconbox-center">
                        <div class="boxes-icon " style="font-size:30px;width:80px; height:80px;line-height:80px">
                            <span class="inner-icon"><i class="vc_icon_element-icon flaticon-travel-2"></i></span>
                        </div>
                        <div class="content-inner">
                            <div class="sc-heading article_heading">
                                <h4 class="heading__primary">Beautiful Places</h4>
                            </div>
                            <div class="desc-icon-box">
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column col-sm-3">
                    <div class="widget-icon-box widget-icon-box-base iconbox-center">
                        <div class="boxes-icon circle" style="font-size:30px;width:80px; height:80px;line-height:80px">
                            <span class="inner-icon"><i class="vc_icon_element-icon flaticon-travelling"></i></span>
                        </div>
                        <div class="content-inner">
                            <div class="sc-heading article_heading">
                                <h4 class="heading__primary">Passionate Travel</h4>
                            </div>
                            <div class="desc-icon-box">
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="padding-top-6x padding-bottom-6x section-background" style="background-image:url(assets/front/images/home/bg-popular.jpg)">
            <div class="container">
                <div class="shortcode_title text-white title-center title-decoration-bottom-center">
                    <div class="title_subtitle">Take a Look at Our</div>
                    <h3 class="title_primary">MOST POPULAR PACKAGES</h3>
                    <span class="line_after_title" style="color:#ffffff"></span>
                </div>
                <div class="row wrapper-tours-slider">
                    <div class="tours-type-slider list_content" data-dots="true" data-nav="true" data-responsive='{"0":{"items":1}, "480":{"items":2}, "768":{"items":2}, "992":{"items":3}, "1200":{"items":4}}'>
                        @foreach ($packages as $element)
                        <div class="item-tour">
                            <div class="item_border">
                                <div class="item_content">
                                    <div class="post_images">
                                        <a href="single-tour.html" class="travel_tour-LoopProduct-link">
                                        <img width="430" height="305" src="{{ asset('core/storage/uploads/images/package/'.$element->cover_image) }}" alt="{{ $element->name }}" title="{{ $element->name }}">
                                        </a>
                                    </div>
                                    <div class="wrapper_content">
                                        <div class="post_title">
                                            <h4>
                                                <a href="{{ url('package/'.$element->slug) }}" rel="bookmark">{{ $element->name }}</a>
                                            </h4>
                                        </div>
                                        <span class="post_date">{{ $element->duration }}</span>
                                        <div class="description">
                                            <p>{!! substr(strip_tags($element->description), 0 , 32) !!}..</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="read_more">
                                    <div class="item_rating">
                                        @for ($i = 0; $i < $element->stars ; $i++)
                                        <i class="fa fa-star"></i>
                                        @endfor
                                        @for ($i = 0; $i <  5 - $element->stars; $i++)
                                        <i class="fa fa-star-o"></i>
                                        @endfor
                                    </div>
                                    <a href="{{ url('package/'.$element->slug) }}" class="read_more_button">VIEW MORE
                                    <i class="fa fa-long-arrow-right"></i></a>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="section-white padding-top-6x padding-bottom-6x tours-type">
            <div class="container">
                <div class="shortcode_title title-center title-decoration-bottom-center">
                    <div class="title_subtitle">Find a Tour by</div>
                    <h3 class="title_primary">DESTINATION</h3>
                    <span class="line_after_title"></span>
                </div>
                <div class="wrapper-tours-slider wrapper-tours-type-slider">
                    <div class="tours-type-slider" data-dots="true" data-nav="true" data-responsive='{"0":{"items":1}, "480":{"items":2}, "768":{"items":3}, "992":{"items":4}, "1200":{"items":5}}'>
                        @foreach ($destinations as $element)
                        <div class="tours_type_item">
                            <a href="tours.html" class="tours-type__item__image">
                            <img width="430" height="305" src="{{ asset('core/storage/uploads/images/destination/'.$element->cover_image) }}" alt="{{ $element->title }}" title="{{ $element->title }}">
                            </a>
                            <div class="content-item">
                                <div class="item__title">{{ $element->title }}</div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="padding-top-6x padding-bottom-6x bg__shadow section-background" style="background-image:url(assets/front/images/home/bg-pallarax.jpg)">
            <div class="container">
                <div class="shortcode_title text-white title-center title-decoration-bottom-center">
                    {{--
                    <div class="title_subtitle">Some statistics about Travel WP</div>
                    <h3 class="title_primary">CENTER ACHIEVEMENTS</h3>
                    <span class="line_after_title" style="color:#ffffff"></span> --}}
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="stats_counter text-center text-white">
                            <div class="wrapper-icon">
                                <i class="flaticon-airplane"></i>
                            </div>
                            <div class="stats_counter_number">94,532</div>
                            <div class="stats_counter_title">Customers</div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="stats_counter text-center text-white">
                            <div class="wrapper-icon">
                                <i class="flaticon-island"></i>
                            </div>
                            <div class="stats_counter_number">1021</div>
                            <div class="stats_counter_title">Destinations</div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="stats_counter text-center text-white">
                            <div class="wrapper-icon">
                                <i class="flaticon-globe"></i>
                            </div>
                            <div class="stats_counter_number">100</div>
                            <div class="stats_counter_title">Packages</div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="stats_counter text-center text-white">
                            <div class="wrapper-icon">
                                <i class="flaticon-people"></i>
                            </div>
                            <div class="stats_counter_number">5</div>
                            <div class="stats_counter_title">Hotels</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-white padding-top-6x padding-bottom-6x">
            <div class="container">
                <div class="shortcode_title title-center title-decoration-bottom-center">
                    <h3 class="title_primary">DEALS AND DISCOUNTS</h3>
                    <span class="line_after_title"></span>
                </div>
                <div class="row wrapper-tours-slider">
                    <div class="tours-type-slider list_content" data-dots="true" data-nav="true" data-responsive='{"0":{"items":1}, "480":{"items":2}, "768":{"items":3}, "992":{"items":3}, "1200":{"items":4}}'>
                        @foreach ($packages as $element)
                        <div class="item-tour">
                            <div class="item_border">
                                <div class="item_content">
                                    <div class="post_images">
                                        <a href="{{ url('package/'.$element->slug) }}" class="travel_tour-LoopProduct-link">
                                        <span class="price">
                                        <span class="travel_tour-Price-amount amount">OFFER</span>
                                        </span>
                                        <img width="430" height="305" src="{{ asset('core/storage/uploads/images/package/'.$element->cover_image) }}" alt="{{ $element->name }}" title="{{ $element->name }}">
                                        </a>
                                    </div>
                                    <div class="wrapper_content">
                                        <div class="post_title">
                                            <h4>
                                                <a href="{{ url('package/'.$element->slug) }}" rel="bookmark">{{ $element->name }}</a>
                                            </h4>
                                        </div>
                                        <span class="post_date">{{ $element->duration }}</span>
                                        <div class="description">
                                            <p>{!! substr(strip_tags($element->description), 0 , 32) !!}..</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="read_more">
                                    <div class="item_rating">
                                        @for ($i = 0; $i < $element->stars ; $i++)
                                        <i class="fa fa-star"></i>
                                        @endfor
                                        @for ($i = 0; $i <  5 - $element->stars; $i++)
                                        <i class="fa fa-star-o"></i>
                                        @endfor
                                    </div>
                                    <a href="{{ url('package/'.$element->slug) }}" class="read_more_button">VIEW MORE
                                    <i class="fa fa-long-arrow-right"></i></a>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="bg__shadow padding-top-6x padding-bottom-6x section-background" style="background-image:url(assets/front/images/home/bg-pallarax.jpg)">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-sm-2"></div> -->
                    <div class="col-sm-12">
                        <div class="discounts-tour">
                            <h3 class="title_primary" style="color:#ffffff">RECOMMENDED HOTELS</h3>
                            <span class="line" style="color:#ffffff"></span>
                            <div class="row wrapper-tours-slider">
                                <div class="tours-type-slider list_content" data-dots="true" data-nav="true" data-responsive='{"0":{"items":1}, "480":{"items":2}, "768":{"items":3}, "992":{"items":3}, "1200":{"items":4}}'>
                                    @foreach ($hotels as $element)
                                    <div class="item-tour">
                                        <div class="item_border">
                                            <div class="item_content">
                                                <div class="post_images">
                                                    <a href="{{ url('hotel/'.$element->slug) }}" class="travel_tour-LoopProduct-link">
                                                    <img width="430" height="305" src="{{ asset('core/storage/uploads/images/hotel/'.$element->cover_image) }}" alt="{{ $element->title }}" title="{{ $element->title }}">
                                                    </a>
                                                </div>
                                                <div class="wrapper_content">
                                                    <div class="post_title">
                                                        <h4>
                                                            <a class="pull-left" href="{{ url('package/'.$element->slug) }}" rel="bookmark">{{ $element->title }}</a>
                                                        </h4>
                                                    </div>
                                                  	<br/>
                                                    <div class="description">
                                                        <p style="text-align: left!important; font-family: Roboto, Helvetica, Arial, sans-serif!important; font-size: 14px!important; font-weight: 400!important; line-height: 24px!important; color: #555!important;">{!! substr(strip_tags($element->description), 0 , 32) !!}..</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="read_more">
                                                <a href="{{ url('hotel/'.$element->slug) }}" class="read_more_button">VIEW MORE
                                                <i class="fa fa-long-arrow-right"></i></a>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-sm-12 text-center padding-top-2x">
                                <a href="{{ url('hotels') }}" class="icon-btn"><i class="flaticon-airplane-4"></i> MORE </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2"></div>
                </div>
            </div>
        </div>
        <div class="section-white padding-top-6x padding-bottom-6x">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="shortcode_title title-center title-decoration-bottom-center">
                            <h2 class="title_primary">Client Reviews</h2>
                            <span class="line_after_title"></span>
                        </div>
                        <div class="shortcode-tour-reviews wrapper-tours-slider">
                            <div class="tours-type-slider" data-autoplay="true" data-dots="true" data-nav="false" data-responsive='{"0":{"items":1}, "480":{"items":1}, "768":{"items":1}, "992":{"items":1}, "1200":{"items":1}}'>
                                @foreach ($reviews as $element)
                                <div class="tour-reviews-item">
                                    <div class="reviews-item-info">
                                        <!-- <img alt="" src="images/avata.jpg" class="avatar avatar-95 photo" height="90" width="90"> -->
                                        <div class="reviews-item-info-name">{{ $element->name }}</div>
                                        <div class="reviews-item-rating">
                                            @for ($i = 0; $i < $element->rating ; $i++)
                                            <i class="fa fa-star"></i>
                                            @endfor
                                            @for ($i = 0; $i <  5 - $element->rating; $i++)
                                            <i class="fa fa-star-o"></i>
                                            @endfor
                                        </div>
                                    </div>
                                    <div class="reviews-item-content">
                                        {{--
                                        <h3 class="reviews-item-title">
                                            <a href="{{ url('package/'.$element->slug) }}">{{ $element->package->name }}</a>
                                        </h3>
                                        --}}
                                        <div class="reviews-item-description">{{ $element->message }}</div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

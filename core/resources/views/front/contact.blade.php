@extends('layouts.front.master') @section('title','Welcome | www.princeofgalle.com')
@section('css')
	<link rel="stylesheet" href="{{asset('assets/front/css/rating.css')}}" type="text/css" media="all">

@stop
@section('content')
	<div class="site wrapper-content">
		<div class="top_site_main" style="background-image:url(assets/front/images/banner/top-heading.jpg);">
			<div class="banner-wrapper container article_heading">
				<div class="breadcrumbs-wrapper">
					<ul class="phys-breadcrumb">
						<li><a href="{{ url('/') }}" class="home">Home</a></li>
						<li>Contact</li>
					</ul>
				</div>
				<h1 class="heading_primary">Contact</h1></div>
		</div>
		<section class="content-area">
			<div class="container">
				<div class="row">
					<div class="site-main col-sm-12 alignleft">
						<div class="video-container">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6304.829986131271!2d-122.4746968033092!3d37.80374752160443!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808586e6302615a1%3A0x86bd130251757c00!2sStorey+Ave%2C+San+Francisco%2C+CA+94129!5e0!3m2!1sen!2sus!4v1435826432051" width="600" height="450" style="border: 0; pointer-events: none;" allowfullscreen=""></iframe>
						</div>
						<div class="pages_content padding-top-4x">
							<h4>CONTACT INFORMATION</h4>
							<div class="contact_infor">
								<ul>
									<li><label><i class="fa fa-map-marker"></i>ADDRESS</label>
										<div class="des">Taman Pertama, Cheras, 56100 Kuala Lumpur, Malaysia</div>
									</li>
									<li><label><i class="fa fa-phone"></i>TEL NO</label>
										<div class="des">+012345678 (tour) | +0123456789 (ticketing)</div>
									</li>
									<li><label><i class="fa fa-print"></i>FAX NO</label>
										<div class="des">+012345678 (tour) | +123456789 (ticketing)</div>
									</li>
									<li><label><i class="fa fa-envelope"></i>EMAIL</label>
										<div class="des">tours@physcode.com (tour) | tickets@physcode.com (ticketing)</div>
									</li>
									<li>
										<label><i class="fa fa-clock-o"></i>WORKING HOURS</label>
										<div class="des">Mon – Fri 9:00 am – 5:30 pm, Sat 9:00 am – 1:00 pm
											<br>
											We are closed on 1st &amp; 3rd Sat of every month, Sundays &amp; public holidays
										</div>
									</li>
									<li>
										<label><i class="fa fa-map-marker"></i>GPS COORDINATE</label>
										<div class="des">Latitude : 3.1117181000, Longitude : 101.7301577000</div>
									</li>
								</ul>
							</div>
						</div>
						<div class="wpb_wrapper pages_content">
							<h4>Have a question?</h4>
							<div role="form" class="wpcf7">
								<div class="screen-reader-response"></div>

								@if (session()->has('msg'))
									<div class="alert alert-success">
										{{session()->get('msg')}}
									</div>
								@endif
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul class="">
												@foreach ($errors->all() as $error)
														<li>{{ $error }}</li>
												@endforeach
										</ul>
									</div>
								@endif
								<form  method="post" action="{{ url('contact') }}" class="wpcf7-form">
									{{ csrf_field() }}
									<div class="form-contact">
										<div class="row-1x">
											<div class="col-sm-6">
													<span class="wpcf7-form-control-wrap your-name">
														<input type="text" name="name" value="" size="40" class="wpcf7-form-control" placeholder="Your name*">
													</span>
											</div>
											<div class="col-sm-6">
													<span class="wpcf7-form-control-wrap your-email">
														<input type="email" name="email" value="" size="40" class="wpcf7-form-control" placeholder="Email*">
													</span>
											</div>
										</div>
										<div class="row-1x">
											<div class="col-sm-6">
													<span class="wpcf7-form-control-wrap your-subject">
														<select class="wpcf7-form-control" name="type">
															{{-- <option value="0" disabled selected>Select Your Inquiry Type</option> --}}
															<option value="general">General Inquiry</option>
															<option value="feedback">Feedback</option>
														</select>
													</span>
											</div>
											<div class="col-sm-6">
													<span class="wpcf7-form-control-wrap your-email">
														<input type="number" name="contact" value="" size="40" class="wpcf7-form-control" placeholder="Contact no.">
													</span>
											</div>
										</div>

										<div class="row-1x">
											<div class=" form-contact-fields" id="feedbacks" style="display:none;margin-bottom: 5em;">

													<div class="col-md-6">
														<label>Your Rating</label>
														<div id="star-rating">
																<input type="radio" name="rating" class="rating" value="1" />
																<input type="radio" name="rating" class="rating" value="2" />
																<input type="radio" name="rating" class="rating" value="3" />
																<input type="radio" name="rating" class="rating" value="4" />
																<input type="radio" name="rating" class="rating" value="5" />
														</div>
													</div>
											</div>
										</div>


										<div class="form-contact-fields">
											<span class="wpcf7-form-control-wrap your-message">
												<textarea name="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" placeholder="Message"></textarea>
												 </span><br>
											<input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="widget-area col-sm-3 align-left">
						<aside class="widget widget_text">
							<img src="images/images-sidebar/sidebanner-ticketing.jpg" alt="">
						</aside>
						<aside class="widget widget_text">
							<img src="images/images-sidebar/sidebanner-tour.png" alt="">
						</aside>
						<aside class="widget widget_text">
							<img src="images/images-sidebar/hertz-sidebanner.jpg" alt="">
						</aside>
					</div>
				</div>
			</div>
		</section>
	</div>
@stop
@section('scripts')


	<script type='text/javascript' src='{{ asset('assets/front/js/rating.js') }}'></script>

	<script type="text/javascript">

		jQuery('#star-rating').rating();

		jQuery('select[name="type"]').change(function(event) {
			if (jQuery(this).val() === "feedback") {
				jQuery('#feedbacks').show()
			}else {
				jQuery('#feedbacks').hide()
			}
		});
	</script>
@endsection

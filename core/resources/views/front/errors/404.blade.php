@extends('layouts.front.master') @section('title','Prince Hotel | Home')
@section('css')

@stop

@section('content')
    <!-- BREADCRUMBS -->
    <section class="breadcrumb parallax margbot30"></section>
    <!-- //BREADCRUMBS -->


    <!-- TYPOGRAPHY BLOCK -->
    <section class="typography_block padbot40">

        <!-- CONTAINER -->
        <div class="container">
          <div id="columns" class="columns-container">
            <!-- container -->
            <div class="container">
              <div class="row">
                <div id="center_column" class="col-lg-12 col-md-12">
                  <div class="page-not-found" align="center">
                    <img src="{{asset('assets/front/images/404 error.png')}}" alt="" class="img-responsive content-center">
                    <h3 class="title_block">Oops! The page you're looking for is unavailable. If you think this is an error, Please contact Administrators!</h3>
                    <p class="page-not-des">It looks like nothing was found at this location. Maybe try to use a search?</p>
                    <a href="{{url('/')}}" class="btn button btn-primary margin-bottom-20">Go Back to homepage</a>
                    <!-- <meta http-equiv="refresh" content="3;url=http://www.doogee.lk"/> -->
                  </div><!-- end page-not-found -->
                </div><!-- end center_column -->
              </div>
            </div> <!-- end container -->
          </div><!--end columns -->
        </div>
    </section>

@stop

@extends('layouts.front.master') @section('title','Welcome | www.princeofgalle.com')
@section('css')
@stop
@section('content')
	<div class="site wrapper-content">
		<div class="top_site_main" style="color: rgb(255, 255, 255); background-color: rgb(0, 0, 0); background-image: url({{asset('assets/front/images/brazil.jpg')}})">
			<div class="banner-wrapper-destination container article_heading text-center">
				<h1 class="heading_primary">Tour Island Sri Lanka</h1>
				<div class="desc"><p>Discover the Sri Lanka with our special tours</p>
				</div>
				{{-- <div class="breadcrumbs-wrapper">
					<ul class="phys-breadcrumb">
						<li><a href="index-2.html" class="home">Home</a></li>
						<li><a href="tours.html" title="Tours">Tours</a></li>
						<li>Brazil</li>
					</ul>
				</div> --}}
			</div>
		</div>

	</div>
@stop
